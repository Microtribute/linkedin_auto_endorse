# Linkedin auto endorsements
Tech stack: Python >= 3.8, Selenium, Chrome Web Driver

## How to use
Make virtualenv, enter to virtualenv, install dependencies, install chrome web driver and than paste this command in terminal end see short movie :)
```bash
python endorse.py -u YOUR_USERNAME -p YOUR_PASSWORD -l LINK_TO_LINKEDIN_MEMBER
```