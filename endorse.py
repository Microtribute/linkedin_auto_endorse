import getopt
import sys
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


def setup():
    input_params = getopt.getopt(sys.argv[1:], "u:p:l")
    if len(input_params[0]) != 3:
        print(
            "Correct input is 'python endorse.py -u username -p -password -l link_to_linkedin_member"
        )
        sys.exit(1)

    username = input_params[0][0][1]
    password = input_params[0][1][1]
    link_to_linkedin_member = input_params[1][0]
    delay_in_seconds = 3
    driver = webdriver.Chrome()
    return username, password, link_to_linkedin_member, delay_in_seconds, driver


def login_to_linkedin(driver, user_name, password):
    driver.get("https://www.linkedin.com/")
    element = driver.find_element_by_id("session_key")
    element.send_keys(user_name)
    element = driver.find_element_by_id("session_password")
    element.send_keys(password)
    element.send_keys(Keys.RETURN)


def scroll_to_show_more_button_and_press_it(driver):
    tmp = 0
    for next_height in range(
        PREVIOUS_SCROLL_HEIGHT := 0, MAX_SCROLL_HEIGHT := 1600, SCROLL_STEP := 200
    ):
        tmp += SCROLL_STEP
        driver.execute_script(f"window.scrollTo({PREVIOUS_SCROLL_HEIGHT}, {tmp})")
    sleep(DELAY)

    # Find "Show more" button
    show_more_button = driver.find_elements_by_class_name(
        "pv-skills-section__additional-skills"
    )

    # Scroll to it
    driver.execute_script(
        f"window.scrollTo({show_more_button[0].location['y']},{show_more_button[0].location['y']-150})"
    )

    show_more_button[0].click()


def perform_endorse(endorse_button):
    """
    If skill is endorsed - we must unendorse and endorse it again.
    It action invoke popup with endorsements descriptions - we use it like
    'succcess message' that we endorse skill :)

    Args:
        endorse_button ([selenium element]): [button that we must press until skill will be endorsed]
    """
    driver.execute_script(
        f"window.scrollTo({endorse_button.location['y']},{endorse_button.location['y']-150})"
    )
    endorse_button.click()
    sleep(DELAY)
    try:
        close_modal_button = driver.find_element_by_class_name(
            "artdeco-hoverable-content__close-btn"
        )
        close_modal_button.click()
    except Exception:
        perform_endorse(endorse_button)


if __name__ == "__main__":

    user_name, password, linkedin_user, DELAY, driver = setup()

    login_to_linkedin(driver, user_name, password)

    # Go to linkedin user`s page
    driver.get(linkedin_user)
    sleep(DELAY)

    # Scroll to skills list (we need this coz Linkedin dynamicly loads this info)
    scroll_to_show_more_button_and_press_it(driver)

    # Find endorsements buttons
    endorsement_buttons = driver.find_elements_by_class_name(
        "pv-skill-entity__featured-endorse-button-shared"
    )
    sleep(DELAY)

    # Endorse skills one by one
    for endorse_button in endorsement_buttons:
        perform_endorse(endorse_button)
